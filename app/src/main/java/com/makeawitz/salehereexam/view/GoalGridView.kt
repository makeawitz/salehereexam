package com.makeawitz.salehereexam.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.makeawitz.salehereexam.R
import com.makeawitz.salehereexam.constant.GoalType

class GoalGridView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var goalSelected = false
        private set

    var goalType = GoalType.TRAVEL
        private set
    private val itemView = LayoutInflater.from(context).inflate(R.layout.goal_grid_item, null, false)

    init {
        itemView.findViewById<TextView>(R.id.goalTitle).setText(goalType.titleResId)
        itemView.findViewById<ImageView>(R.id.goalIcon).setImageResource(goalType.iconResId)
        itemView.setBackgroundResource(R.drawable.round_red_border_white_bg)
        addView(itemView)
    }

    fun setData(goalType: GoalType) {
        this.goalType = goalType
        itemView.findViewById<TextView>(R.id.goalTitle).setText(goalType.titleResId)
        itemView.findViewById<ImageView>(R.id.goalIcon).setImageResource(goalType.iconResId)
        itemView.setBackgroundResource(R.drawable.round_red_border_white_bg)
        invalidate()
    }

    fun toggleSelect(callback: (item: GoalGridView) -> Unit) {
        if (goalSelected) {
            clearSelect()
            callback(this)
        } else {
            goalSelected = true
            itemView.setBackgroundResource(R.drawable.round_green_border_white_bg)
            callback(this)
        }
    }

    fun clearSelect() {
        goalSelected = false
        itemView.setBackgroundResource(R.drawable.round_red_border_white_bg)
    }
}