package com.makeawitz.salehereexam.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.makeawitz.salehereexam.R
import com.makeawitz.salehereexam.constant.GoalStatus
import com.makeawitz.salehereexam.constant.GoalType
import com.makeawitz.salehereexam.data.GoalData
import com.makeawitz.witzcraft.screen.CalculatedTextSize
import com.makeawitz.witzcraft.screen.ScreenMetrics

class GoalItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var savingGoal: Int = 0
    private var savingCurrent: Int = 0
    private var durationLeft: Int = 0
    private var goalType: GoalType = GoalType.TRAVEL
    private var goalName: String = ""
    private val screenMetrics = ScreenMetrics(context)
    private val calculatedSize = CalculatedTextSize(screenMetrics)

    private val goalStatus: GoalStatus
        get() = when {
            durationLeft == 0 && savingCurrent == savingGoal -> GoalStatus.GOOD
            durationLeft == 0 -> GoalStatus.UNHAPPY
            (savingGoal - savingCurrent) / savingGoal.toDouble() < 0.5 -> GoalStatus.GOOD
            else -> GoalStatus.UNHAPPY
        }
    private val size = (screenMetrics.screenWidth * 0.4).toInt()
    private val barWidth = (size * 0.9).toInt()
    private val cBarWidth: Int
        get() = if (savingGoal == 0) barWidth else savingCurrent * barWidth / savingGoal

    private val barHeight = resources.getDimensionPixelOffset(R.dimen.barHeight)

    private val icon = ImageView(context)
    private val goalSaving = TextView(context)
    private val currentSaving = TextView(context)
    private val goalTitle = TextView(context)
    private val goalStatusText = TextView(context)
    private val durationLeftText = TextView(context)
    private val maxBar = View(context)
    private val currentBar = View(context)

    constructor(context: Context, goalData: GoalData) : this(context) {
        savingGoal = goalData.goalSaving
        savingCurrent = goalData.currentSaving
        durationLeft = goalData.duration
        goalType = goalData.goalType
        goalName = goalData.title

        icon.setImageResource(goalType.iconResId)
        goalSaving.text = "$savingGoal ${resources.getString(R.string.currency)}"
        currentSaving.text = "$savingCurrent ${resources.getString(R.string.currency)}"
        goalTitle.text = goalName
        with(goalStatusText) {
            setText(goalStatus.nameResId)
            setTextColor(ContextCompat.getColor(context, goalStatus.colorResId))
        }
        durationLeftText.text = "$durationLeft ${resources.getString(R.string.days_left)}"
        currentBar.layoutParams = LayoutParams(cBarWidth, barHeight)
        setBackgroundResource(goalStatus.backgroundResId)
    }

    init {
        layoutParams = ViewGroup.LayoutParams(size, size)

        val iconSize = (size * 0.27).toInt()
        val iconParams = LayoutParams(iconSize, iconSize)
        with(icon) {
            layoutParams = iconParams
            setImageResource(goalType.iconResId)
            scaleType = ImageView.ScaleType.FIT_CENTER
            adjustViewBounds = true
        }

        val goalSavingParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        with(goalSaving) {
            layoutParams = goalSavingParams
            text = "$savingGoal ${resources.getString(R.string.currency)}"
            textSize = calculatedSize.regular
        }

        val currentSavingParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        with(currentSaving) {
            layoutParams = currentSavingParams
            text = "$savingCurrent ${resources.getString(R.string.currency)}"
            textSize = calculatedSize.subHeader
            setTextColor(ContextCompat.getColor(context, R.color.buttonRed))
        }

        val goalTitleParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        with(goalTitle) {
            layoutParams = goalTitleParams
            text = goalName
            textSize = calculatedSize.subHeader
        }

        val goalStatusParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        with(goalStatusText) {
            layoutParams = goalStatusParams
            setText(goalStatus.nameResId)
            textSize = calculatedSize.regular
            setTextColor(ContextCompat.getColor(context, goalStatus.colorResId))
        }

        val durationParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        with(durationLeftText) {
            layoutParams = durationParams
            text = "$durationLeft ${resources.getString(R.string.days_left)}"
            textSize = calculatedSize.regular
        }

        val maxBarParams = LayoutParams(barWidth, barHeight)
        with(maxBar) {
            layoutParams = maxBarParams
            setBackgroundResource(R.drawable.black_bar)
        }

        val currentBarParams = LayoutParams(cBarWidth, barHeight)
        with(currentBar) {
            layoutParams = currentBarParams
            setBackgroundResource(R.drawable.red_bar)
        }

        addView(icon)
        addView(goalSaving)
        addView(currentSaving)
        addView(goalTitle)
        addView(goalStatusText)
        addView(durationLeftText)
        addView(maxBar)
        addView(currentBar)
    }

    fun setData(goalData: GoalData) {
        savingGoal = goalData.goalSaving
        savingCurrent = goalData.currentSaving
        durationLeft = goalData.duration
        goalType = goalData.goalType
        goalName = goalData.title

        icon.setImageResource(goalType.iconResId)
        goalSaving.text = "$savingGoal ${resources.getString(R.string.currency)}"
        currentSaving.text = "$savingCurrent ${resources.getString(R.string.currency)}"
        goalTitle.text = goalName
        with(goalStatusText) {
            setText(goalStatus.nameResId)
            setTextColor(ContextCompat.getColor(context, goalStatus.colorResId))
        }
        durationLeftText.text = "$durationLeft ${resources.getString(R.string.days_left)}"
        currentBar.layoutParams = LayoutParams(cBarWidth, barHeight)

        setBackgroundResource(goalStatus.backgroundResId)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {x
        if (!changed) {
            return
        }
        val leftBarrier = left + (size * 0.05).toInt()
        val rightBarrier = right - (size * 0.05).toInt()
        val iconTop = top + (size * 0.1).toInt()
        icon.layout(
            leftBarrier,
            iconTop,
            leftBarrier + icon.measuredWidth,
            iconTop + icon.measuredHeight
        )

        val cSaveTop = top + (size * 0.1).toInt()
        currentSaving.layout(
            rightBarrier - currentSaving.measuredWidth,
            cSaveTop,
            rightBarrier,
            cSaveTop + currentSaving.measuredHeight
        )

        val gSaveTop = cSaveTop + currentSaving.measuredHeight + (size * 0.01).toInt()
        goalSaving.layout(
            rightBarrier - goalSaving.measuredWidth,
            gSaveTop,
            rightBarrier,
            gSaveTop + goalSaving.measuredHeight
        )

        val barTop = iconTop + icon.measuredHeight + (size * 0.05).toInt()
        maxBar.layout(
            leftBarrier,
            barTop,
            leftBarrier + maxBar.measuredWidth,
            barTop + barHeight
        )

        currentBar.layout(
            leftBarrier,
            barTop,
            leftBarrier + currentBar.measuredWidth,
            barTop + barHeight
        )

        val titleTop = barTop + barHeight + (size * 0.05).toInt()
        goalTitle.layout(
            leftBarrier,
            titleTop,
            leftBarrier + goalTitle.measuredWidth,
            titleTop + goalTitle.measuredHeight
        )

        val statusLeft = leftBarrier + (size * 0.01).toInt()
        val bottomBarrier = bottom - (size * 0.1).toInt()
        goalStatusText.layout(
            statusLeft,
            bottomBarrier - goalStatusText.measuredHeight,
            statusLeft + goalStatusText.measuredWidth,
            bottomBarrier
        )

        durationLeftText.layout(
            rightBarrier - durationLeftText.measuredWidth,
            bottomBarrier - durationLeftText.measuredHeight,
            rightBarrier,
            bottomBarrier
        )
    }
}