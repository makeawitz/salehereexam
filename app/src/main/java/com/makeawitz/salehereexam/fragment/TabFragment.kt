package com.makeawitz.salehereexam.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.makeawitz.salehereexam.R

class TabFragment(
    private val layoutResId: Int,
    private val onViewCreatedCallback: (tabFragment: TabFragment) -> Unit = {}
) : Fragment() {

    lateinit var frame: FrameLayout
        private set

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.tab_view, container, false)
        frame = rootView.findViewById(R.id.frame)
        frame.addView(inflater.inflate(layoutResId, null, false))
        onViewCreatedCallback(this)
        return rootView
    }
}