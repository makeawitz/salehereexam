package com.makeawitz.salehereexam.data

import com.makeawitz.salehereexam.constant.GoalType

object MockData {

    // Mock data
    const val achievementLevel = 2

    fun getMockGoalData(): MutableList<GoalData> {
        val goal1 = GoalData(
            GoalType.TRAVEL,
            20000,
            16500,
            "ไปเที่ยวญี่ปุ่น",
            45
        )

        val goal2 = GoalData(
            GoalType.INVEST,
            6000,
            500,
            "ซื้อกองทุนรวม",
            20
        )

        val goal3 = GoalData(
            GoalType.TRAVEL,
            30000,
            20500,
            "ไปทะเล",
            12
        )

        return mutableListOf(goal1, goal2, goal3)
    }
}