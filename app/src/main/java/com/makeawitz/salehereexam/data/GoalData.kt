package com.makeawitz.salehereexam.data

import com.makeawitz.salehereexam.constant.GoalType

class GoalData(
    val goalType: GoalType,
    val goalSaving: Int,
    var currentSaving: Int,
    val title: String,
    val duration: Int
)