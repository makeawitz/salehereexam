package com.makeawitz.salehereexam

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.github.nkzawa.socketio.client.IO
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.makeawitz.salehereexam.adapter.*
import com.makeawitz.salehereexam.data.MockData
import com.makeawitz.salehereexam.fragment.TabFragment
import com.makeawitz.salehereexam.view.GoalGridView

class MainActivity : AppCompatActivity() {

    val spanCount = 3
    private val socket = IO.socket("https://px-socket-api.herokuapp.com/")

    private lateinit var homeDetailView: View
    private lateinit var goalDetailView: View
    private var isHomeShowDetail = false
    private var currentPageTrack = 0

    private var isNewGoalSet = false

    private val offerAdapter1 = OfferAdapter(mutableListOf(R.drawable.mock1, R.drawable.mock2, R.drawable.mock3))
    private val offerAdapter2 =
        OfferAdapter(mutableListOf(R.drawable.mock4, R.drawable.mock5, R.drawable.mock6, R.drawable.mock7))

    private val goalAdapter = GoalAdapter(this, MockData.getMockGoalData())
    private val goalDetailAdapter = GoalDetailAdapter(this)

    private val home1SectionAdapter = Section1stAdapter(this, goalAdapter)
    private val home2SectionAdapter = Section2ndAdapter(this)
    private val home3SectionAdapter = Section3rdAdapter(offerAdapter1)
    private val home4SectionAdapter = Section4thAdapter(offerAdapter2)


    private val viewHome = TabFragment(R.layout.view_home) {
        homeDetailView = layoutInflater.inflate(R.layout.view_home_detail, null, false)
        goalDetailView = layoutInflater.inflate(R.layout.view_goal_detail, null, false)
        showHomePage()
        initHomeDetail()
    }

    private val viewAchievement = TabFragment(R.layout.view_achievement) { tabFragment ->
        tabFragment.frame.findViewById<TextView>(R.id.level).text =
            "${getString(R.string.level)} ${MockData.achievementLevel}"
        val recyclerView = tabFragment.frame.findViewById<RecyclerView>(R.id.achievementRecyclerView)
        val achievementAdapter = AchievementAdapter(this, 8, spanCount)
        with(recyclerView) {
            addItemDecoration(
                GridSpacingDecoration(achievementAdapter.decoSpace, spanCount)
            )
            layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, spanCount)
            adapter = achievementAdapter
            overScrollMode = View.OVER_SCROLL_NEVER
        }
    }

    private val blank1 = TabFragment(R.layout.blank_view)
    private val blank2 = TabFragment(R.layout.blank_view)

    private lateinit var tabAdapter: TabAdapter
    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager = findViewById(R.id.pager)
        tabLayout = findViewById(R.id.tabs)
        tabAdapter = TabAdapter(supportFragmentManager, lifecycle)
        tabAdapter.addTab(viewHome, R.drawable.home_icon_state)
        tabAdapter.addTab(blank1, R.drawable.travel_icon_state)
        tabAdapter.addTab(viewAchievement, R.drawable.badge_icon_state)
        tabAdapter.addTab(blank2, R.drawable.person_icon_state)

        viewPager.adapter = tabAdapter
        viewPager.setPageTransformer(MarginPageTransformer(1500))
        viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        viewPager.isUserInputEnabled = false

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.setIcon(tabAdapter.getIconResId(position))
        }.attach()
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPageTrack = position
                if (position == 3) {
                    val badge = tabLayout.getTabAt(3)?.orCreateBadge
                    badge?.number = 0
                    badge?.isVisible = false
                }
            }
        })

        initEventListener()
    }

    override fun onBackPressed() {
        if (currentPageTrack == 0 && isHomeShowDetail) {
            showHomePage()
            return
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        socket.disconnect()
        socket.off()
    }

    private fun initEventListener() {
        socket.on("new-notification") { event ->
            runOnUiThread {
                val badge = tabLayout.getTabAt(3)?.orCreateBadge
                badge?.number = event.size
                badge?.isVisible = true//event.isNotEmpty()
            }
        }
        socket.connect()
    }

    private fun initHomeDetail() {
        val recyclerView = homeDetailView.findViewById<RecyclerView>(R.id.homeRecyclerView)

        val concatAdapter =
            ConcatAdapter(home1SectionAdapter, home2SectionAdapter, home3SectionAdapter, home4SectionAdapter)
        with(recyclerView) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = concatAdapter
            overScrollMode = View.OVER_SCROLL_NEVER
        }

    }

    private fun showHomePage() {
        viewHome.frame.removeAllViewsInLayout()
        viewHome.frame.addView(homeDetailView)
        isHomeShowDetail = false
    }

    fun showNewGoalPage() {
        isHomeShowDetail = true
        viewHome.frame.removeAllViewsInLayout()
        viewHome.frame.addView(goalDetailView)
        if (!isNewGoalSet) {
            initGoalDetail()
            isNewGoalSet = true
        }
    }

    private fun initGoalDetail() {
        val recyclerView = goalDetailView.findViewById<RecyclerView>(R.id.recyclerView)

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = goalDetailAdapter
            overScrollMode = View.OVER_SCROLL_NEVER
        }
    }

    fun selectNewGoalType(goal: GoalGridView?) {
        goalDetailAdapter.goalType = goal?.goalType
    }

    fun clearNewGoalData() {
        goalDetailView.findViewById<EditText>(R.id.goalTitleInput).setText("")
        goalDetailAdapter.goalGridAdapter.deselectedAll()
    }

    fun addGoal() {
        val goalTitleInput = goalDetailView.findViewById<EditText>(R.id.goalTitleInput)
        val goalData = goalDetailAdapter.getGoalData(goalTitleInput.text.toString()) ?: return
        goalAdapter.addItem(goalData)
        home1SectionAdapter.notifyDataSetChanged()
        onBackPressed()
    }

}
