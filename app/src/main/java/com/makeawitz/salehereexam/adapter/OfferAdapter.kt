package com.makeawitz.salehereexam.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.R

class OfferAdapter(private val list: MutableList<Int>) : RecyclerView.Adapter<OfferAdapter.CardViewHolder>() {

    fun setItems(newList: List<Int>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    fun addItem(data: Int) {
        list.add(data)
        notifyDataSetChanged()
    }

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.offer_item, parent, false)
        return CardViewHolder(v)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        if (list.isEmpty()) {
            return
        }
        holder.itemView.findViewById<ImageView>(R.id.image).setImageResource(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}