package com.makeawitz.salehereexam.adapter

import android.app.DatePickerDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.MainActivity
import com.makeawitz.salehereexam.R
import com.makeawitz.salehereexam.constant.GoalType
import com.makeawitz.salehereexam.data.GoalData
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

class GoalDetailAdapter(private val activity: MainActivity) :
    RecyclerView.Adapter<GoalDetailAdapter.CardViewHolder>() {

    private val size = 1
    var goalType: GoalType? = null
    private var amount = 0
    private var selectedDateInstant: Instant? = null

    lateinit var goalGridAdapter: GoalGridAdapter
        private set

    private lateinit var recyclerView: RecyclerView

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card) {
        val spinner: Spinner = card.findViewById(R.id.accountSpinner)
        private val form: LinearLayout = card.findViewById(R.id.form)
        val amountBlock: View = form.findViewById(R.id.amountBlock)
        val amountInput: EditText = form.findViewById(R.id.amountInput)
        val dateText: TextView = form.findViewById(R.id.dateText)
        val dateBlock: View = form.findViewById(R.id.dateBlock)
        val accountBlock: View = form.findViewById(R.id.accountBlock)
        val perMonthBlock: View = form.findViewById(R.id.perMonthBlock)
        val perMonthInput: EditText = form.findViewById(R.id.perMonthInput)
        val button: View = form.findViewById(R.id.button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.goal_detail_item, null, false)
        return CardViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val itemView = holder.itemView
        val gridRecyclerView = itemView.findViewById<RecyclerView>(R.id.gridRecyclerView)

        goalGridAdapter = GoalGridAdapter(
            activity,
            listOf(GoalType.TRAVEL, GoalType.EDUCATION, GoalType.INVEST, GoalType.CLOTHING, GoalType.EDUCATION),
            activity.spanCount
        )
        with(gridRecyclerView) {
            addItemDecoration(
                GridSpacingDecoration(goalGridAdapter.decoSpace, activity.spanCount)
            )
            layoutManager = GridLayoutManager(context, activity.spanCount)
            adapter = goalGridAdapter
            overScrollMode = View.OVER_SCROLL_NEVER
        }

        holder.spinner.adapter =
            BankSpinnerAdapter(
                activity,
                R.layout.dropdown_item,
                activity.resources.getStringArray(R.array.accounts).asList()
            )

        holder.amountBlock.setOnClickListener {
            holder.amountInput.performClick()
        }

        holder.dateBlock.setOnClickListener {
            val datePicker = DatePickerDialog(activity)
            datePicker.setOnDateSetListener { _, year, month, dayOfMonth ->
                val dateFormat = SimpleDateFormat("dd MMM yyyy")
                selectedDateInstant =
                    LocalDate.of(year, month + 1, dayOfMonth).atStartOfDay(ZoneId.systemDefault()).toInstant()
                holder.dateText.text = dateFormat.format(Date.from(selectedDateInstant))
            }
            datePicker.show()
        }

        holder.accountBlock.setOnClickListener {
            holder.spinner.performClick()
        }

        holder.perMonthBlock.setOnClickListener {
            holder.perMonthInput.performClick()
        }

        holder.button.setOnClickListener {
            val amountString = holder.amountInput.text.toString()
            if (amountString.isBlank()) {
                showInvalidInput()
                return@setOnClickListener
            }
            amount = amountString.toInt()
            activity.addGoal()
            clearAll()
        }
    }

    fun getGoalData(title: String): GoalData? {
        if (goalType == null) {
            return showInvalidInput()
        }

        if (selectedDateInstant == null) {
            return showInvalidInput()
        }
        val duration = ((selectedDateInstant!!.epochSecond - Instant.now().epochSecond) / (60 * 60 * 24)).toInt()
        if (duration < 0) {
            return showInvalidInput()
        }

        return GoalData(goalType!!, amount, 0, title, duration)
    }

    private fun showInvalidInput(): GoalData? {
        Toast.makeText(activity, R.string.invalid_input, Toast.LENGTH_LONG).show()
        clearAll()
        return null
    }

    private fun clearAll() {
        activity.clearNewGoalData()
        val holder = (recyclerView.findViewHolderForAdapterPosition(0) ?: return) as CardViewHolder
        holder.amountInput.setText("")
        holder.dateText.text = ""
        holder.spinner.setSelection(0)
        holder.perMonthInput.setText("")
    }

    override fun getItemCount(): Int {
        return size
    }

}