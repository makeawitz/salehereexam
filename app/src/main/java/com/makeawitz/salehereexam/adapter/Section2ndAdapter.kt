package com.makeawitz.salehereexam.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.MainActivity
import com.makeawitz.salehereexam.R

class Section2ndAdapter(private val activity: MainActivity) :
    RecyclerView.Adapter<Section2ndAdapter.CardViewHolder>() {

    private val size = 1

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.home_2nd_section, parent, false)
        return CardViewHolder(v)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            activity.showNewGoalPage()
        }
    }

    override fun getItemCount(): Int {
        return size
    }

}