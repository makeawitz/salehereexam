package com.makeawitz.salehereexam.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.R
import com.makeawitz.witzcraft.screen.ScreenMetrics

class AchievementAdapter(context: Context, private val size: Int, spanCount: Int) :
    RecyclerView.Adapter<AchievementAdapter.CardViewHolder>() {

    private val screenMetrics = ScreenMetrics(context)
    private val itemSize = (screenMetrics.screenWidth / (spanCount + 0.5)).toInt()
    val decoSpace: Int = ((screenMetrics.screenWidth - (itemSize * spanCount)) / (spanCount + 1))

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.grid_achievement, parent, false)

        val item = v.findViewById<View>(R.id.item)
        val params = item.layoutParams
        params.width = itemSize
        params.height = itemSize
        return CardViewHolder(v)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return size
    }

}