package com.makeawitz.salehereexam.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.MainActivity
import com.makeawitz.salehereexam.R
import com.makeawitz.salehereexam.constant.GoalType
import com.makeawitz.salehereexam.view.GoalGridView
import com.makeawitz.witzcraft.screen.ScreenMetrics

class GoalGridAdapter(private val activity: MainActivity, private val list: List<GoalType>, spanCount: Int) :
    RecyclerView.Adapter<GoalGridAdapter.CardViewHolder>() {

    private lateinit var recyclerView: RecyclerView

    private val screenMetrics = ScreenMetrics(activity)
    private val itemSize = (screenMetrics.screenWidth / (spanCount + 0.5)).toInt()
    val decoSpace: Int = ((screenMetrics.screenWidth - (itemSize * spanCount)) / (spanCount + 1))

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = GoalGridView(parent.context)

        val item = v.findViewById<View>(R.id.item)
        val params = item.layoutParams
        params.width = itemSize
        params.height = itemSize
        return CardViewHolder(v)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        if (list.isEmpty()) {
            return
        }

        val itemView = holder.itemView as GoalGridView
        itemView.setData(list[position])
        itemView.setOnClickListener {
            val v = it as GoalGridView
            deselectedAllExcept(v)
            v.toggleSelect {
                if (v.goalSelected) {
                    activity.selectNewGoalType(v)
                } else {
                    activity.selectNewGoalType(null)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun deselectedAllExcept(v: GoalGridView) {
        for (position in list.indices) {
            val item = recyclerView.findViewHolderForAdapterPosition(position)?.itemView as GoalGridView
            if (item == v) {
                continue
            }
            item.clearSelect()
        }
    }

    fun deselectedAll() {
        for (position in list.indices) {
            val item = recyclerView.findViewHolderForAdapterPosition(position)?.itemView as GoalGridView
            item.clearSelect()
        }
    }

}