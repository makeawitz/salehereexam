package com.makeawitz.salehereexam.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.makeawitz.salehereexam.fragment.TabFragment

class TabAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private val fragmentList: MutableList<TabFragment> = ArrayList()
    private val iconResIdList: MutableList<Int> = ArrayList()

    fun addTab(fragment: TabFragment, iconResId: Int) {
        fragmentList.add(fragment)
        iconResIdList.add(iconResId)
    }

    fun getIconResId(position: Int): Int {
        return iconResIdList[position]
    }

    override fun getItemCount(): Int {
        return iconResIdList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }
}