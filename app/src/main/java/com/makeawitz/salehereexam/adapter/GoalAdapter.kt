package com.makeawitz.salehereexam.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.data.GoalData
import com.makeawitz.salehereexam.view.GoalItemView

class GoalAdapter(private val context: Context, private val list: MutableList<GoalData>) : RecyclerView.Adapter<GoalAdapter.CardViewHolder>() {

    fun setItems(newList: List<GoalData>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    fun addItem(data: GoalData) {
        list.add(data)
        notifyDataSetChanged()
    }

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        return CardViewHolder(FrameLayout(parent.context))
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        if (list.isEmpty()) {
            return
        }

        val item = holder.itemView as FrameLayout
        item.removeAllViewsInLayout()
        item.addView(GoalItemView(context, list[position]))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun getTotalSaving(): Int {
        var result = 0
        for (goal in list) {
            result += goal.currentSaving
        }
        return result
    }
}