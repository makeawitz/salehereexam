package com.makeawitz.salehereexam.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class GridSpacingDecoration(private val space: Int, private val spanCount: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.top = if (parent.getChildLayoutPosition(view) < spanCount) space else 0
        outRect.bottom = space
        outRect.left = if (parent.getChildLayoutPosition(view) % spanCount == 0) space else space / 2
        outRect.right = if (parent.getChildLayoutPosition(view) % spanCount == spanCount - 1) space else space / 2
    }
}
