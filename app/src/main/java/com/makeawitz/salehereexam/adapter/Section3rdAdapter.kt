package com.makeawitz.salehereexam.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.R

class Section3rdAdapter(private var offerAdapter: OfferAdapter) :
    RecyclerView.Adapter<Section3rdAdapter.CardViewHolder>() {

    private val size = 1

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.home_3rd_section, parent, false)
        return CardViewHolder(v)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val item = holder.itemView
        val recyclerView = item.findViewById<RecyclerView>(R.id.recyclerView)
        with(recyclerView) {
            addItemDecoration(
                HorizontalListSpacingDecoration(
                    resources.getDimensionPixelOffset(
                        R.dimen.largeMargin
                    )
                )
            )
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = offerAdapter
            overScrollMode = View.OVER_SCROLL_NEVER
        }
        item.findViewById<TextView>(R.id.title).setText(R.string.best_offer)
    }

    override fun getItemCount(): Int {
        return size
    }

}