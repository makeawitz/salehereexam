package com.makeawitz.salehereexam.adapter

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makeawitz.salehereexam.R

class Section1stAdapter(private val context: Context, private var goalAdapter: GoalAdapter) :
    RecyclerView.Adapter<Section1stAdapter.CardViewHolder>() {

    private val size = 1

    fun updateGoal(goalAdapter: GoalAdapter) {
        this.goalAdapter = goalAdapter
        notifyDataSetChanged()
    }

    class CardViewHolder internal constructor(card: View) : RecyclerView.ViewHolder(card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.home_1st_section, parent, false)
        return CardViewHolder(v)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val item = holder.itemView
        val recyclerView = item.findViewById<RecyclerView>(R.id.recyclerView)
        with(recyclerView) {
            addItemDecoration(
                HorizontalListSpacingDecoration(
                    resources.getDimensionPixelOffset(
                        R.dimen.largeMargin
                    )
                )
            )
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = goalAdapter
            overScrollMode = View.OVER_SCROLL_NEVER
        }
        item.findViewById<TextView>(R.id.goals).text =
            "${goalAdapter.itemCount} ${context.resources.getString(R.string.goal_unit)}"

        val allSavingString = context.resources.getString(R.string.total_saving)
        val savingAmount = "${goalAdapter.getTotalSaving()}"
        val currency = context.resources.getString(R.string.currency)
        val combined = "$allSavingString $savingAmount $currency"
        val startAmountIndex = combined.indexOf(savingAmount)
        val lastAmountIndex = startAmountIndex + savingAmount.length
        val spannable = SpannableString(combined)
        spannable.setSpan(RelativeSizeSpan(1.5f), startAmountIndex, lastAmountIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        item.findViewById<TextView>(R.id.saving).text = spannable
    }

    override fun getItemCount(): Int {
        return size
    }

}