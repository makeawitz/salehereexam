package com.makeawitz.salehereexam.constant

import com.makeawitz.salehereexam.R

enum class GoalType(val titleResId: Int, val iconResId: Int) {
    TRAVEL(R.string.travel, R.drawable.travel),
    EDUCATION(R.string.education, R.drawable.education),
    INVEST(R.string.invest, R.drawable.invest),
    CLOTHING(R.string.clothing, R.drawable.clothing)
}