package com.makeawitz.salehereexam.constant

import com.makeawitz.salehereexam.R

enum class GoalStatus(val nameResId: Int, val colorResId: Int, val backgroundResId: Int) {
    GOOD(R.string.good, R.color.borderGreen, R.drawable.round_green_border_white_bg),
    UNHAPPY(R.string.unhappy, R.color.orange, R.drawable.round_red_border_white_bg)
}